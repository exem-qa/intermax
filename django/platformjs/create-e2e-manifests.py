import sys, os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from pathlib import Path
from kubernetes import utils
from kube.reference import k8s_client, namespace_name, vendor

# Directory Create OK
def create_directory(dir_path):
    Path(dir_path).mkdir(parents=True, exist_ok=True)

# e2e setting
def e2e_create_manifest_set(dg_type, set_name, e2e_resources, setting):
    for e2e_resource in e2e_resources :
        fr = open("templates/{}/{}-{}-templates.yml".format(dg_type, e2e_resource, setting), 'r')
      #  fr = open("templates/{}/{}-{}-templates.yml".format(dg_type, e2e2_resource, setting), 'r')
        
        templateLines = fr.readlines()
        fr.close()

        e2e_create_manifest(dg_type, templateLines, set_name, e2e_resource, setting)        

# e2e2_setting
def e2e_create_manifest(dg_type, lines, set_name, e2e_resourceName, settingName):
    dir_path = "{}/{}".format(dg_type, set_name)
    create_directory(dir_path)

    filePath = "{}/{}-{}-{}.yml".format(dir_path, e2e_resourceName, set_name, settingName)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", set_name).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))

# e2e setting
def e2e2_create_manifest_set(dg_type, set_name, e2e2_resources):
    for e2e2_resource in e2e2_resources :
        fr = open("templates/{}/{}-templates.yml".format(dg_type, e2e2_resource), 'r')
        
        templateLines = fr.readlines()
        fr.close()

        e2e2_create_manifest(dg_type, templateLines, set_name, e2e2_resource)        

# e2e2_setting
def e2e2_create_manifest(dg_type, lines, set_name, e2e2_resourceName):
    dir_path = "{}/{}".format(dg_type, set_name)
    create_directory(dir_path)

    filePath = "{}/{}-{}.yml".format(dir_path, e2e2_resourceName, set_name)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", set_name).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))    

def main():
    ## set 고정
    dg_type = "e2e"
    set_names = ["xset"]
    e2e_resources = ["configmap"]
    settings = ["menu", "option", "configjson"]
    e2e_create_sets(dg_type, set_names, e2e_resources, settings)
    
    ## set2 고정
    dg_type = "e2e"
    set_names = ["xset"]
    e2e2_resources = ["statefulset"]
    e2e2_create_sets(dg_type, set_names, e2e2_resources)

# e2e    
def e2e_create_sets(dg_type, set_names, e2e_resources, settings):
    for set_name in set_names :
        for setting in settings : 
            e2e_create_manifest_set(dg_type, set_name, e2e_resources, setting)   

# e2e2
def e2e2_create_sets(dg_type, set_names, e2e2_resources):
    for set_name in set_names :
        e2e2_create_manifest_set(dg_type, set_name, e2e2_resources)      
                    
if __name__ == '__main__':
     main()
