import sys, os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from pathlib import Path
from kubernetes import utils
from kube.reference import k8s_client, namespace_name, vendor

# Directory Create OK
def create_directory(dir_path):
    Path(dir_path).mkdir(parents=True, exist_ok=True)

# setting
def create_manifest_set(dg_type, set_name, resources, setting):
    for resource in resources :
        fr = open("templates/{}/{}-{}-templates.yml".format(dg_type, resource, setting), 'r')
      #  fr = open("templates/{}/{}-{}-templates.yml".format(dg_type, two_resource, setting), 'r')
        
        templateLines = fr.readlines()
        fr.close()

        create_manifest(dg_type, templateLines, set_name, resource, setting)        

# setting
def create_manifest(dg_type, lines, set_name, _resourceName, settingName):
    dir_path = "{}/{}".format(dg_type, set_name)
    create_directory(dir_path)

    filePath = "{}/{}-{}-{}.yml".format(dir_path, _resourceName, set_name, settingName)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", set_name).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))

# two_setting
def two_create_manifest_set(dg_type, set_name, two_resources):
    for two_resource in two_resources :
        fr = open("templates/{}/{}-templates.yml".format(dg_type, two_resource), 'r')
        
        templateLines = fr.readlines()
        fr.close()

        two_create_manifest(dg_type, templateLines, set_name, two_resource)        

# two_setting
def two_create_manifest(dg_type, lines, set_name, two_resourceName):
    dir_path = "{}/{}".format(dg_type, set_name)
    create_directory(dir_path)

    filePath = "{}/{}-{}.yml".format(dir_path, two_resourceName, set_name)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", set_name).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))    

def main():

 ####### APM
     
    ## apm 고정
    dg_type = "apm"
    set_names = ["aset","bset", "cset", "dset"]
    resources = ["configmap"]
    settings = ["menu", "option", "configjson"]
    create_sets(dg_type, set_names, resources, settings)
    
    ## apm 고정
    dg_type = "apm"
    set_names = ["aset","bset", "cset", "dset"]
    two_resources = ["statefulset", "service"]
    two_create_sets(dg_type, set_names, two_resources)

 ####### E2E

    ## e2e 고정
    dg_type = "e2e"
    set_names = ["xset"]
    resources = ["configmap"]
    settings = ["menu", "option", "configjson"]
    create_sets(dg_type, set_names, resources, settings)
    
    ## e2e 고정
    dg_type = "e2e"
    set_names = ["xset"]
    two_resources = ["statefulset", "service"]
    two_create_sets(dg_type, set_names, two_resources)    
   
def create_sets(dg_type, set_names, _resources, settings):
    for set_name in set_names :
        for setting in settings : 
            create_manifest_set(dg_type, set_name, _resources, setting)   

def two_create_sets(dg_type, set_names, two_resources):
    for set_name in set_names :
        two_create_manifest_set(dg_type, set_name, two_resources)     
                    
if __name__ == '__main__':
     main()
