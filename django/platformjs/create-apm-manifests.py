import sys, os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from pathlib import Path
from kubernetes import utils
from kube.reference import k8s_client, namespace_name, vendor

# Directory Create OK
def create_directory(dir_path):
    Path(dir_path).mkdir(parents=True, exist_ok=True)

# apm_setting
def apm_create_manifest_set(dg_type, set_name, apm_resources, setting):
    for apm_resource in apm_resources :
        fr = open("templates/{}/{}-{}-templates.yml".format(dg_type, apm_resource, setting), 'r')
      #  fr = open("templates/{}/{}-{}-templates.yml".format(dg_type, apm2_resource, setting), 'r')
        
        templateLines = fr.readlines()
        fr.close()

        apm_create_manifest(dg_type, templateLines, set_name, apm_resource, setting)        

# apm2_setting
def apm_create_manifest(dg_type, lines, set_name, apm_resourceName, settingName):
    dir_path = "{}/{}".format(dg_type, set_name)
    create_directory(dir_path)

    filePath = "{}/{}-{}-{}.yml".format(dir_path, apm_resourceName, set_name, settingName)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", set_name).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))

# apm_setting
def apm2_create_manifest_set(dg_type, set_name, apm2_resources):
    for apm2_resource in apm2_resources :
        fr = open("templates/{}/{}-templates.yml".format(dg_type, apm2_resource), 'r')
        
        templateLines = fr.readlines()
        fr.close()

        apm2_create_manifest(dg_type, templateLines, set_name, apm2_resource)        

# apm2_setting
def apm2_create_manifest(dg_type, lines, set_name, apm2_resourceName):
    dir_path = "{}/{}".format(dg_type, set_name)
    create_directory(dir_path)

    filePath = "{}/{}-{}.yml".format(dir_path, apm2_resourceName, set_name)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", set_name).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))    

def main():
    ## set 고정
    dg_type = "apm"
    set_names = ["aset","bset","cset","dset"]
    apm_resources = ["configmap"]
    settings = ["menu", "option", "configjson"]
    apm_create_sets(dg_type, set_names, apm_resources, settings)
    
    ## set2 고정
    dg_type = "apm"
    set_names = ["aset","bset","cset","dset"]
    apm2_resources = ["statefulset", "service"]
    apm2_create_sets(dg_type, set_names, apm2_resources)

# apm    
def apm_create_sets(dg_type, set_names, apm_resources, settings):
    for set_name in set_names :
        for setting in settings : 
            apm_create_manifest_set(dg_type, set_name, apm_resources, setting)   

# apm2
def apm2_create_sets(dg_type, set_names, apm2_resources):
    for set_name in set_names :
        apm2_create_manifest_set(dg_type, set_name, apm2_resources)      
                    
if __name__ == '__main__':
     main()
