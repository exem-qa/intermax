#!python
print("Content-Type: form_ex/html")
print()
from kubernetes import client
from .kube_configuration import configuration  

# Config
global k8s_client
k8s_client = client.ApiClient(configuration=configuration)

# Namespace_name 
global namespace_name

namespace_name = "intermax-e2e" 

# namespace_name = request.form['id_name']
# namespace_name = str(namespace_name)

# print(namespace_name)

# Verdor_name
global vendor
vendor = "e2e"
