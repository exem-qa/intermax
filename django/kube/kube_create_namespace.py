import time, logging, traceback
from kube_configuration import configuration
from kubernetes import dynamic
from kubernetes.client import api_client
from reference import namespace_name

def create_namespace(namespace_api, name):   
    namespace_manifest = {
        "apiVersion": "v1",
        "kind": "Namespace",
        "metadata": {"name": name, "resourceversion": "v1"},
    }
    namespace_api.create(body=namespace_manifest)

def delete_namespace(namespace_api, name):
    namespace_api.delete(name=name)

def main():
    # Load local config
    client = dynamic.DynamicClient(
        api_client.ApiClient(configuration=configuration)
    )
 
    namespace_api = client.resources.get(api_version="v1", kind="Namespace")
    
    # Creating a namespace
    if namespace_name != [] :
        logging.info('Namespace {} not found. Creating it now.'.format(namespace_name))
        try:    
            create_namespace(namespace_api, namespace_name)
            time.sleep(4)
            print("\n[INFO] namespace: " + namespace_name + " created")

        except :#Exception as e: 
                logging.error("Unable to create namespace in cluster! {}".format(namespace_name + " namespace already have"))
                logging.debug(traceback.format_exc())
                #raise e
               
        #else:
        #    return namespace_name[0] 

if __name__ == '__main__':
    main()