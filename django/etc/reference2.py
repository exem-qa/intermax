from kubernetes import client
from kube_configuration2 import configuration

# Namespace_name 
global namespace_name
namespace_name = "hi" 

# Verdor_name
global vendor
vendor = "e2e"

global k8s_client
k8s_client = client.ApiClient(configuration=configuration)
