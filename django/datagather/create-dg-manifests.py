import sys, os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from pathlib import Path
from kubernetes import utils
from kube.reference import k8s_client, namespace_name, vendor

# Directory Create OK
def create_directory(dir_path):
    Path(dir_path).mkdir(parents=True, exist_ok=True)

# Resource Type Set
def create_manifest_set(dg_type, set_name, resources, gather):
    for resource in resources :
        fr = open("templates/{}/{}-{}-templates.yml".format(dg_type, resource, gather), 'r')
        
        templateLines = fr.readlines()
        fr.close()

        create_manifest(dg_type, templateLines, set_name, resource, gather)    

# Manifset Create
def create_manifest(dg_type, lines, set_name, resourceName, gatherName):
    dir_path = "{}/{}".format(dg_type, set_name)
    create_directory(dir_path)

    filePath = "{}/{}-{}-{}.yml".format(dir_path, resourceName, set_name, gatherName)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", set_name).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))

def main():
    ## set 고정
    dg_type = "apm"
    set_names = ["aset","bset"]
    resources = ["configmap", "statefulset", "service"]
    gathers = ["master", "slave"]
    create_sets(dg_type, gathers, set_names, resources)

    ## => 덮어쓰기
    dg_type = "e2e"
    set_names = ["xset"]
    resources = ["configmap", "statefulset", "service"]
    gathers = ["master", "slave"]
    create_sets(dg_type, gathers, set_names, resources)
    
def create_sets(dg_type, gathers, set_names, resources):
    for set_name in set_names :
        for gather in gathers :
            create_manifest_set(dg_type, set_name, resources, gather)   
    
if __name__ == '__main__':
     main()
