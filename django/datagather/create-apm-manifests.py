import sys, os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from kubernetes import utils
from kube.reference import k8s_client, namespace_name, vendor

def create_manifest_set(setName, resources, gather):
    for resource in resources :
        fr = open("apm_templates/{}-{}-templates.yml".format(resource, gather), 'r')
        
        templateLines = fr.readlines()
        fr.close()

        lines = templateLines
        create_manifest(lines, setName, resource, gather)    

def create_manifest(lines, setName, resourceName, gatherName):
    filePath = "apm/{}/{}-{}-{}.yml".format(setName, resourceName, setName, gatherName)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", setName).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))

def main():
    
    ## set 고정
    set_names = ["aset","bset","cset","dset"]
    resources = ["configmap", "statefulset", "service"]
    gathers = ["master", "slave"]

    for set_name in set_names :
        for gather in gathers :
            create_manifest_set(set_name, resources, gather)   

if __name__ == '__main__':
     main()
