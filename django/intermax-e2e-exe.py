import os

JSPD = 'jspd/'
DG = '../datagather/'
PJS = '../platformjs/'

# JSPD exe
os.chdir(JSPD)

exec(open("create-manifests.py").read())

# DG exe
os.chdir(DG)

exec(open("create-dg-manifests.py").read())

# pjs e2e
os.chdir(PJS)

exec(open("create-pjs-manifests.py").read())

# print(os.getcwd())

# print(os.path.abspath('.'))