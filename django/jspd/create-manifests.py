import sys, os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from pathlib import Path
from kubernetes import utils
from kube.reference import k8s_client, namespace_name, vendor

# Directory Create OK
def create_directory(dir_path):
    Path(dir_path).mkdir(parents=True, exist_ok=True)

# Resource Type Set
def create_manifest_set(jspd_type, set_name, resources):
    for resource in resources:
        fr = open("templates/{}-templates.yml".format(resource), 'r')

        templateLines = fr.readlines()
        fr.close()

        create_manifest(jspd_type, templateLines, set_name, resource)

# Manifset Create
def create_manifest(jspd_type, lines, set_name, resourceName):
    dir_path = "{}/".format(jspd_type)
    create_directory(dir_path)

    filePath = "{}/{}-{}.yml".format(dir_path, resourceName, set_name)
    fw = open(filePath, 'w')
    replacedlines = ''
    for line in lines:    
        replacedlines += line.replace("{{ setname }}", set_name).replace("{{ namespace }}", namespace_name).replace("{{ vendor }}", vendor)

    fw.write(replacedlines)
    fw.close()

    objects = utils.create_from_yaml(k8s_client, filePath, namespace=namespace_name)
    print("Agent-Manifest {0} created".format(objects))

def main():
    ## set 고정
    jspd_type = "yaml"
    set_names = ["aset","bset"]
    resources = ["configmap", "statefulset"]
    create_sets(jspd_type, set_names, resources)

def create_sets(jspd_type, set_names, resources):
    for set_name in set_names:
        create_manifest_set(jspd_type, set_name, resources)

if __name__ == '__main__':
     main()